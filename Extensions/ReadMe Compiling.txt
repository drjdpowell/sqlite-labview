Installed MinGW (had problems; had to uninstall/reinstall)


SQLite AMALGAM header files and extension-functions.c in same dir

cmd terminal
cd to this directory,

gcc -g -m32 -shared extension-functions.c -o extension-functions.dll

-m32 is for 32 bit


reference
https://sqlite.org/loadext.html
https://www.sqlite.org/contrib/download/


********April 2019 work***********
-g is debugging option --> don't need

If Compiling SQLite (for if downloaded precompile binaries on SQLite.org not enough):

Get sqlite-amalgamation from https://sqlite.org/download.html

cmd terminal
cd to this directory

gcc -m32 -DSQLITE_ENABLE_JSON1 -DSQLITE_ENABLE_RTREE -DSQLITE_ENABLE_FTS4 -DSQLITE_OMIT_DEPRECATED -shared sqlite3.c -o sqlite3.dll

Many other compile options at https://sqlite.org/compile.html

********** June, 2022 ***********
https://sourceforge.net/projects/mingw-w64/files/   --> 64 bit compiler
https://sourceforge.net/projects/mingw/   -- 32-bit compiler

commands for regular expresions from SQLean:
gcc -shared src/sqlite3-re.c src/re.c -o dist/re.dll
C:\mingw64\bin\gcc -shared src/sqlite3-re.c src/re.c -o dist/re.dll
C:\mingw64\bin\gcc -m32 -shared -I. src/sqlite3-re.c src/re.c -o dist/re_32.dll

Commands for new regex from SQLean:
C:\mingw64\bin\gcc -shared -DPCRE2_CODE_UNIT_WIDTH=8 -DLINK_SIZE=2 -DHAVE_CONFIG_H -DSUPPORT_UNICODE -DPCRE2_STATIC src\sqlite3-regexp.c src\regexp\regexp.c src\regexp\pcre2\*.c -o dist\regexp.dll
C:\mingw64\bin\gcc -m32 -shared -I. -DPCRE2_CODE_UNIT_WIDTH=8 -DLINK_SIZE=2 -DHAVE_CONFIG_H -DSUPPORT_UNICODE -DPCRE2_STATIC src\sqlite3-regexp.c src\regexp\regexp.c src\regexp\pcre2\*.c -o dist\regexp_32.dll

Commands for define
C:\mingw64\bin\gcc -shared src/sqlite3-define.c src/define/*.c -o dist/define.dll
C:\mingw64\bin\gcc -m32 -shared -I. src/sqlite3-define.c src/define/*.c -o dist/define_32.dll
